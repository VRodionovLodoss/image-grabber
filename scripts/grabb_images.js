'use strict'

const recursive = require('recursive-readdir');
const path = require('path');
const _ = require('lodash');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const Image = mongoose.model('Image');

// TODO: need to move it into config
const workFolder = 'public/images';

function ignoreFunc(file, stats) {
  // `file` is the absolute path to the file, and `stats` is an `fs.Stats`
  // object returned from `fs.lstat()`.
  return stats.isDirectory();
}

const Reader = {};

Reader.readFiles = function (cb) {
  const pathToServer = path.join(__dirname, '..');
  const fullPath = path.join(pathToServer, workFolder);
  console.log('we are here', workFolder, fullPath);
  // Ignore files named 'foo.cs' and descendants of directories named test
  recursive(fullPath, [], function (err, files) {
    // Files is an array of filename
    files = _.map(files, (file, key) => {
      const filePath = file.replace(fullPath, '')
      const subfolder = path.dirname(filePath);
      const name = filePath.replace(subfolder + '/', '').replace(/\//g, '');
      return new Image({
        path: filePath,
        fullPath: file,
        subfolder,
        name,
        index: key,
      });
    })
    // console.log(files);
    return cb(files);
  });
};

Reader.saveFiles = function (files) {
  console.log('run when complete', files.length);
  const promises = _.map(files, (file) => {
    let update = Promise.promisify(function(obj, cb) {
      obj = obj.toObject();
      delete obj._id;
      let query = { subfolder: obj.subfolder, name: obj.name };
      // TODO: not all object, created only if not existed
      Image.update(query, obj, { upsert: true }, function(err, result) {
        if (err) {
          cb(err);
        } else {
          cb(null, result);
        }
      });
    });
    return update(file);
  });
  Promise.all(promises)
    .then((results) => {
      console.log('saving completed');
    })
    .catch((err) => {
      console.log('saving failed', err);
    });
};

module.exports = Reader;
