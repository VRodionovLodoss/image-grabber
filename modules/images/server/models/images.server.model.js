'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Image Schema
 */
var ImageSchema = new Schema({
  index: {
    type: Number,
    default: 0,
  },
  name: {
    type: String,
    default: '',
    trim: true,
  },
  subfolder: {
    type: String,
    default: '',
    trim: true,
  },
  status: {
    type: String,
    default: 'Active',
  },
  archived: {
    type: String,
    default: '',
  },
  points: {
    type: Number,
    default: 1,
  },
});

mongoose.model('Image', ImageSchema);
