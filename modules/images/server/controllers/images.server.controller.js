'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Image = mongoose.model('Image'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
const fs = require('fs');
// TODO: need to move it into config
const ArchivedFolder = '/public/archived';
const workFolder = 'public/images';

/**
 * Show the current image
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var image = req.image ? req.image.toJSON() : {};

  res.json(image);
};

/**
 * Delete an image
 */
exports.delete = function (req, res) {
  var image = req.image;

  // TODO: ADD functionality to move image into archive folder

  Image.update({ index: {$lt: image.index}, status: 'Active' }, { $inc: { points: 1 }}, { multi: true }, function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {

      Image.update({ _id: req.params.imageId }, { points: 0, subfolder: '', status: 'Archived'}, function (err) {
        if (err) {
          return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          const pathToServer = path.join(__dirname, '..', '..', '..', '..');
          const fullPath = path.join(pathToServer, workFolder, image.subfolder, image.name);
          const newPath = path.join(pathToServer, ArchivedFolder, image.name);
          fs.rename(fullPath, newPath, function (err) {
            console.log('fs.rename', arguments);
            if (err) {
              return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
              });
            } else {
              return res.json(image);
            }
          });
        }
      });
    }
  });
};

/**
 * List of images
 */
exports.list = function (req, res) {
  const skip = parseInt(req.query.offset, 10) || 0;
  const limit = parseInt(req.query.limit, 10) || 10;
  const filter = req.query.filter;
  const query = filter == 'more' ? { status: 'Active' } : { points: 0 };
  Image.find(query).skip(skip).limit(limit).sort('index').exec(function (err, images) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(images);
    }
  });
};

/**
 * Image middleware
 */
exports.imageByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Image is invalid'
    });
  }

  Image.findById(id).exec(function (err, image) {
    if (err) {
      return next(err);
    } else if (!image) {
      return res.status(404).send({
        message: 'No image with that identifier has been found'
      });
    }
    req.image = image;
    next();
  });
};
