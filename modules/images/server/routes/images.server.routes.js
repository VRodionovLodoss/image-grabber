'use strict';

/**
 * Module dependencies
 */
var imagesPolicy = require('../policies/images.server.policy'),
  images = require('../controllers/images.server.controller');

module.exports = function (app) {
  // Images collection routes
  app.route('/api/images').all(imagesPolicy.isAllowed)
    .get(images.list)

  // Single image routes
  app.route('/api/images/:imageId').all(imagesPolicy.isAllowed)
    .get(images.read)
    .delete(images.delete);

  // Finish by binding the image middleware
  app.param('imageId', images.imageByID);
};
