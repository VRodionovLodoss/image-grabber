(function () {
  'use strict';

  angular
    .module('images.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('images', {
        abstract: true,
        url: '/images',
        template: '<ui-view/>'
      })
      .state('images.list', {
        url: '',
        templateUrl: '/modules/images/client/views/list-images.client.view.html',
        controller: 'ImagesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Images List'
        }
      });
  }

  getImage.$inject = ['$stateParams', 'ImagesService'];

  function getImage($stateParams, ImagesService) {
    return ImagesService.get({
      imageId: $stateParams.imageId
    }).$promise;
  }
}());
