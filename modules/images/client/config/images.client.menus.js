(function () {
  'use strict';

  angular
    .module('images')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Images',
      state: 'images',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'images', {
      title: 'List Images',
      state: 'images.list',
      roles: ['*']
    });
  }
}());
