(function () {
  'use strict';

  angular
    .module('images')
    .controller('ImagesListController', ImagesListController);

  ImagesListController.$inject = ['ImagesService'];

  function ImagesListController(ImagesService) {
    var vm = this;
    vm.filter = 'more';
    vm.images = [];
    // vm.images = ImagesService.query({ offset: vm.images.length });


    vm.getMore = function () {
      const images = ImagesService.query({ offset: vm.images.length, filter: vm.filter }).$promise.then((result) => {
        vm.images = [].concat(vm.images, result);
      });
    };

    vm.applyFilter = function () {
      vm.images = [];
      vm.getMore();
    };

    vm.archive = function(image) {
      image.$remove(function () {
        vm.images.forEach((item, key) => {
          if (item.index < image.index) {
            item.points++;
          }
          if (item._id.toString() == image._id.toString()) {
            vm.images.splice(key, 1);
          }
        })
      });
    };
  }
}());
