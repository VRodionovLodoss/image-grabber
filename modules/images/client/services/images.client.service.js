(function () {
  'use strict';

  angular
    .module('images.services')
    .factory('ImagesService', ImagesService);

  ImagesService.$inject = ['$resource', '$log'];

  function ImagesService($resource, $log) {
    var Image = $resource('/api/images/:imageId', {
      imageId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    // angular.extend(Image.prototype, {
    //   createOrUpdate: function () {
    //     var image = this;
    //     return createOrUpdate(image);
    //   }
    // });

    return Image;

    function createOrUpdate(image) {
      if (image._id) {
        return image.$update(onSuccess, onError);
      } else {
        return image.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(image) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
